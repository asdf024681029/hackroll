# Training Exam on AWS!

## 靜態網頁發生錯誤！

### 情境

- 使用者架設一靜態網頁於 AWS **東京區域**。

- 某天網頁卻出現了錯誤，需要 Roll Back 回正常網頁。

- 請使用者幫忙解決。

- 當正常解決問題後會得到 Token，可將 Token 輸入於 EC2 上的計分板（使用 EC2 Public IP 連線）。




